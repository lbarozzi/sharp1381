﻿using System;
using System.Net;
using System.Net.Sockets;
namespace sharp1381 {

    public class ASTMTcpClient {
        protected System.Net.Sockets.TcpClient tcpClient = null;
        public IPEndPoint Target { get; set; }
        protected ASTMTcpClient() {
        }
        public ASTMTcpClient(IPEndPoint iep) {
            this.Target=iep;
            Initialize();
        }
        public ASTMTcpClient(string server, int port) {
            IPHostEntry dst= Dns.Resolve(server);
            this.Target=new IPEndPoint(dst.AddressList[0],port);
            Initialize();
        }
        protected void Initialize() {
            tcpClient= new TcpClient(Target);
            tcpClient.Connect();
        }
    }
}
