﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

namespace sharp1381 {
    public class ASTMTcpServer {
        protected System.Net.Sockets.TcpListener tcpListener = null;
        protected List<Thread> clients;
        protected Thread _main;
        public ASTMTcpServer(int port) 
        {
            this.port = port;
               
        }
                public int port {get;set;}
    
        public ASTMTcpServer(int port=2306,IPAddress ip=null) {
            this.port = port;
            if (ip == null) {
                ip = IPAddress.Any;
            }
            clients= new List<Thread>();
            tcpListener = new TcpListener(ip,port);
            _main = new Thread(new ThreadStart(mainloop));
            //_main.IsBackground=true;
            _main.Start();
        }

        protected virtual void mainloop() {
            tcpListener.Start();
            try {
                for(;;){
                    TcpClient newCli=tcpListener.AcceptTcpClient();
                    newCli.NoDelay=true;
                    Console.WriteLine(String.Format("New Connection from: {0}",newCli.Client.RemoteEndPoint.ToString()) );
                    //*
                    Thread newTh= new Thread(new ParameterizedThreadStart(client_loop));
                    newTh.IsBackground=true;
                    newTh.Name=String.Format("Client for {0}",newCli.Client.RemoteEndPoint.ToString());
                    newTh.Start(newCli);
                    clients.Add(newTh);
                }
                //*/
            } catch (ThreadInterruptedException ) {
                Debug.WriteLine(String.Format("End of Thread {0}",Thread.CurrentThread.Name));
                clients.Remove(Thread.CurrentThread);
            }
        }

        protected virtual void client_loop(object ob_cli){
            TcpClient cli = ob_cli as TcpClient;
            var flux= cli.GetStream();
            var filtro = new ASTM.Astm1381(flux);
            try {
                for(;;){
                    try {
                        var pkt =filtro.ReadPacket();

                        Console.WriteLine(pkt.Replace("\r","\n"));
                        filtro.SendPacket("H|\rP|\rL|\r");
                    } catch(System.IO.IOException iops) {
                        if(cli.Connected) {
                            Console.WriteLine("No Data Read loop again");
                        } else {
                            throw;
                        }
                    }
                }
            } catch(Exception ops){
                cli.Close();
                Console.WriteLine(ops.StackTrace);
            }
        }
    }
}
