using System;

namespace ASTM{
    public interface IDataFilter{
        bool SendPacket(string Data);
        string ReadPacket();
    }
}