using System;
using System.IO;
using System.Text;

namespace ASTM {

    public class Astm1381 : IDataFilter {
        public int AstmMTU { get; set; }
        protected StreamReader Rx;
        protected StreamWriter Tx;
        public enum AstmStates {
            IDLE,
            ESTABLISH,
            CONTENTION,
            TRANSFER,
            EOT
        }
        /*
         * Enumerate by name first 31 ASCII char
         */
        public enum ASCII {
            NUL,SOH,STX,ETX,EOT,ENQ,ACK,
            BEL,BS,HT,LF,VT,FF,CR,SO,SI,
            DLE,DC1,DC2,DC3,DC4,NAK,SYN,ETB,
            CAN,EM,SUB,ESC,FS,GS,RS,US
        }
        public AstmStates Status { get; set; }
        private Stream _stream;
        public int TimeOut { get; set; }
        /* 
         * MTU of 240 bytes is *UNDOCUMENTED*, arbitrary used by Quanta 
         * The ASTM 1381 define a max packet len of 64K, clear related 
         * to 16bit memory address of old CPU and SO or small embedded 
         */
        public Astm1381(Stream In,int mtu=240,int timeout=15000) {
            _stream = In;
            TimeOut = timeout;
            if (!In.CanRead || !In.CanWrite) {
                throw new NotSupportedException("Read/Write Only Steam are not supported");
            }
            In.ReadTimeout = timeout;
            Rx = new StreamReader(In);
            Tx = new StreamWriter(In);
            AstmMTU = mtu;
        }
        #region Tx function
        /*
         * Send a string escaped and fragmented to the stream
         */
        public bool SendPacket(string Data) {
            //this.Status = AstmStates.ESTABLISH;
            int Frame = 0;
            //int CurrentPosition = 0;
            Tx.Write((char)ASCII.ENQ);
            Tx.Flush();
            //* todo: TEST 
            if (!AckNak()) {
                Tx.Write((char)ASCII.EOT);
                Tx.Flush();
                this.Status = AstmStates.IDLE;
                return false;
            }
            //*/
            //AckNak(); 
            this.Status = AstmStates.TRANSFER;
            int OffSet = 0;
            int send_attempt = 0;
            /*  this is from DCA c++ 
            if(len<this->mtu) {
                sendFrame(rawData,0,len,true);
            }else{
                for (int i=0;i*mtu<len;i+=this->mtu){
                    int xlen=(len-(mtu*i))%mtu;
                    sendFrame(&rawData[i*mtu],f_num++,xlen,(len-this->mtu<i));
                }
            }
            sendEOT();
            /*/
            //while (AstmMTU + OffSet < Data.Length) {
            if(Data.Length<AstmMTU) {
                //SendSingleFrame
                SendFrame(Data,0,true);
            } else {
                for(int i=0;i*AstmMTU<Data.Length;i+=AstmMTU){
                    int xlen=(Data.Length-(AstmMTU*i))%AstmMTU;
                    SendFrame(Data.Substring(i*AstmMTU,xlen),Frame++,(xlen<AstmMTU));
                }
            }
            /*
            while(Data.Length-OffSet>=AstmMTU) {
                if (!this.SendFrame(Data.Substring(OffSet, AstmMTU), Frame++)) {
                    if (++send_attempt < 3) {
                        continue;
                    } else {
                        this.Status = AstmStates.IDLE;
                        return false;
                    }
                } else {
                    send_attempt = 0;
                }
                OffSet = AstmMTU * Frame;
            }
            send_attempt=0;
            //LastFrame
            while(true){
                if (!this.SendFrame(Data.Substring(OffSet), Frame++,true)) {
                    if (++send_attempt < 3) {
                        continue;
                    } else {
                        this.Status = AstmStates.IDLE;
                        return false;
                    }
                } else {
                    send_attempt = 0;
                    break;
                }
            }
            //*/
            //Close
            Tx.Write((char)ASCII.EOT);
            Tx.Flush();

            this.Status = AstmStates.IDLE;
            return true;
        }

        protected bool SendFrame(string frame_data, int frame_num, bool final = false) {
            //STX
            //DATA
            //ETB ETX (=FINAL)
            //TAIL CC CC CR LF
            Tx.Write((char)ASCII.STX);
            Tx.Flush();
            string ToSend=string.Format("{0}{1}{2}",(char)frame_num % 7,frame_data,(char)((final) ? ASCII.ETX : ASCII.ETB));
            Tx.Write(string.Format("{0}{1}\r\n", ToSend, CheckSum(ToSend)));
            Tx.Flush();
            return AckNak();
        }
        #endregion
        #region Rx functions
        /* 
         * Get a Packet, verify checksum from the stream
         */
        public string ReadPacket() {
            DateTime start = DateTime.Now;
            TimeSpan TmOut = TimeSpan.FromMilliseconds(this.TimeOut);
            //Waiting timeout for END
            StringBuilder Buffer = new StringBuilder();
            while (this.Status!=AstmStates.EOT && DateTime.Now.Subtract(start)<TmOut) {
                int next = Rx.Read();
                if (next!=(char)ASCII.ENQ){
                    continue;
                } else {
                    //Start 
                    Tx.Write((char)ASCII.ACK);
                    Tx.Flush();
                }
                while (true) {
                    try {
                        this.Status=AstmStates.TRANSFER;
                        Buffer.Append(ReadFrame());
                        if (this.Status==AstmStates.EOT) {
                            next = Rx.Read();      //EOT
                            if(next!=(char)ASCII.EOT){
                                //Some strange
                                throw new Exception(string.Format("Waiting for EOT recv {0:X02}",next));
                            }
                            break;
                        }
                    } catch (IOException) {
                        //IO shit
                        throw;

                    } catch (InvalidDataException) {
                        //Checksum Fail
                        throw;
                    }
                }

                if (DateTime.Now.Subtract(start).TotalSeconds > 15 ) {
                    throw new TimeoutException();
                }
            }
            Status = AstmStates.ESTABLISH;
            //throw new NotImplementedException();
            return Buffer.ToString();
        }
        /*
         * Can Throw IOException os InvalidOperation if chksum fail
         */
        public string ReadFrame() {
            /* Bad think destroy checksum */
            //string localBuf = Rx.ReadLine();    //Every Frame end with cr lf
            //string localBuf="";
            StringBuilder sba=new StringBuilder();
            string ChkSum="";
            while(true){
                int next=(char)Rx.Read();
                if (next==-1 ) {
                    break;
                };
                char nc=(char)next;
                //Skip STX
                if (nc==(char)ASCII.STX) {
                    continue;
                }
                //Append ETX or ETB
                sba.Append(nc);
                if (nc==(char)ASCII.ETB || nc ==(char)ASCII.ETX){
                    ChkSum= Rx.ReadLine();
                    if (nc==(char)ASCII.ETX) {
                        this.Status=AstmStates.EOT;
                    }
                    break;
                }
            }
            /*
            string localBuf= sba.ToString();
            if (localBuf.Length<3) {
                throw new IOException("no enought data for frame");
            }
            //*/
            //string Data = localBuf.Substring(1, localBuf.Length - 2);   // ReadLine Strip cr lf AND SOX
            string Data= sba.ToString();
            //string ChkSum = localBuf.Substring(Data.Length, 2).ToUpper();         // Len useless
            Exception ops = null;
            if (CheckSum(Data).Equals(ChkSum)) {
                Tx.Write((char)ASCII.ACK);
                Tx.Flush();
            } else {
                ops=new  InvalidDataException(string.Format("Checksum fail {0} vs {1}", CheckSum(Data),ChkSum));
                Tx.Write((char)ASCII.NAK);
                Tx.Flush();
            }
            Tx.Flush();
            if (ops != null) {
                //Some bad happens
                throw ops;
            }
            //Remove control char ETX/ETB
            sba.Remove(sba.Length-1,1);
            return sba.ToString()   ;
        }
        #endregion

        #region Utilities
        /* 
         * I rely in stream timeout set in costructor
         */
        protected bool AckNak() {
            int next = Rx.Read();
            Console.WriteLine("Receives  0x{0:X02} ACK = {1:X02} NAK= {2:X02}",next,(int)ASCII.ACK,(int)ASCII.NAK);
            return ((char)next == (char)ASCII.ACK);
        }

        protected string CheckSum(string data) {
            int sumOfChars = 0;
            //idx < strlen(data)
            for (int idx = 0; idx < data.Length; idx++) {
                sumOfChars += data[idx];
            }
            sumOfChars %= (char)256;
            return String.Format("{0:X02}", sumOfChars);
        }
        protected string CheckSum(string data, char end) {
            return CheckSum(string.Format("{0}{1}", data, end));
        }
        #endregion
    }
}